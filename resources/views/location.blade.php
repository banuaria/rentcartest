@extends('layouts.myapp')
@section('content')
    <div class="mx-auto max-w-screen-xl ">
        <div class="flex md:flex-row flex-col justify-around  items-center px-6 pt-6">
            <div class="md:m-12 p-6 md:w-1/2">
                <img loading="lazy" src="/images/tokomobil.jpg" alt="shop image">
            </div>
            <div class=" relative md:m-12 m-6 md:w-1/2 md:p-6">

                <p>
                    Discover the ultimate car rental experience at our centrally situated rental hub. Conveniently located in the heart of the city, our rental hub offers seamless access and serves as the perfect destination for all your car rental requirements. Whether you're a local resident or an adventurous traveler exploring the area, finding us couldn't be easier.</p>
                <br>
                <p>Strategically positioned near key transportation hubs such as airports, train stations, and bus terminals, our rental hub ensures unparalleled convenience for picking up and returning your rental vehicle. Upon your arrival, our dedicated team will extend a warm welcome, guaranteeing a hassle-free and efficient rental experience from beginning to end.</p>
            </div>

        </div>
        <div class=" p-3 mb-8">
            <iframe width="100%" height="600"
                src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=jl%20cikuray+(My%20Business%20Name)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"
                class="w-full h-96" allowfullscreen="" loading="lazy"
                referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div>

    </div>
@endsection
