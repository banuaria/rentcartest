<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Car;
use App\Models\Reservation;
use Barryvdh\DomPDF\Facade\Pdf as PDF;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function invoice($reservation_id)
    {
        $reservation = Reservation::find($reservation_id);

        $pdf = PDF::loadView('invoice', compact('reservation'));
        $filename = 'Reservation-' . $reservation_id.'-invoice'.'.pdf';

        // $pdf->save(storage_path('app/invoices/' . $filename));

        return $pdf->download($filename);
    }

    public function test(Request $request, $reservation_id)
    {
        $request->validate([
            'no_plat' => 'required|string',
        ]);
        $reservation = Reservation::find($reservation_id);
        $carReserve = $reservation->car_id;

        $plateNumber = $request->input('no_plat');
        $car = Car::where('no_plat', $plateNumber)->first();
        if($car){
            $dataCar = $car->id;
        }else{
            $dataCar = 0;
        }

        if($carReserve == $dataCar){
            $car->status = 'Available';
            $reservation->status = 'Ended';
            $car->save();
            $reservation->save();

            return redirect()->back();
        }else{
            return response()->json(['message' => 'Plate number not found.'], 404);
        }

    }
}
